# ce code trace en automatique
# l'histogramme de données uniques
# portées dans un fichier
# il affiche sur le graphe :
#-- la moyenne
#-- l'écart-type
#-- le nbe de mesures
#-- l'incdertitude de répétabilité
# ----------------------------------
import matplotlib.pyplot as plt
import numpy as np
import csv
# ----------------------------------

# ----------------------------------
# placement des valeurs dans une liste
file = open("humidite0.csv", "r")
"""
"""
L=[]
lecteur=csv.reader(file)
for ligne in lecteur : 
    elmt=int(ligne[0])
    L.append(elmt)
file.close()
# ----------------------------------

# ----------------------------------
# intervalles pour l'histo
# nbe conseillé d'intervalles :
N = 2*len(L)**(1/3) # cf iGEN 2021
#print(N)
# pas entre les barres
pas = ((max(L)-min(L))/N)
if pas > 1 :
    pas = int((max(L)-min(L))/N)
else :
    pas=1
#print(pas)
intervBarres=range(min(L), max(L)+pas, pas)
# ----------------------------------

# ----------------------------------
# tracé de l'histo
plt.hist(L, bins=intervBarres,rwidth=0.95)
# ----------------------------------

# ----------------------------------
# calculs de moy et écart-type, nombre et affichage
moyenne = sum(L)/len(L)
s = np.std(L,ddof=1)# écart-type/ ddof pr dénom à n-1 (écart type corrigé)
nombre=len(L)
# arrondi
moyenne=moyenne
texte="moyenne : "+str(moyenne)
texte2=" écart-type : $s =$"+str(s)
texte3="effectif : "+str(nombre)
plt.text(min(L),5,texte)
plt.text((min(L)+max(L))/2,5,texte2)
plt.text(min(L),10,texte3)
# ----------------------------------

# ----------------------------------
# gestion des xticks et labels
# pas entre les labels
pasLabels=2*pas
# le label le plus petit : milieu de la première barre
minLabels = int(min(L)+pas/2)
# le label le + gd : milieu de la dernière barre
maxLabels = int(max(L)+pas/2)
# liste des labels
intervLabels=range(minLabels, maxLabels+pasLabels, pasLabels)
# affichage
plt.xticks(intervLabels)
# ----------------------------------

# ----------------------------------
# gestion des axes, grille, légendes
plt.xlabel("valeurs acquises")
plt.ylabel("fréquences")
valeurMesuree=round(moyenne,1)
u_repetabilite= round(s/nombre**(1/2),4)
plt.title("Valeur Mesurée : "+str(valeurMesuree)+" --- incertitude-type : $u_{\\mathrm{rep}} = \\dfrac{s}{\\sqrt{n}} =$"+str(u_repetabilite))
#plt.legend()
# ----------------------------------
# affichage
plt.show()

