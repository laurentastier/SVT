const int led=8;// on définit une constante "led" qui vaut 8 (broche 8)
void setup() {// la phase de réglages
  pinMode(led, OUTPUT);// la broche 8 (led) est une sortie
}

void loop() {// phase lue en boucle
  digitalWrite(led, HIGH);// on place la broche 8 à l'état haut (led allumée)
  delay(2000);// on attend 2 secondes
  digitalWrite(led, LOW);// on éteint la broche 8 (led éteinte)
  delay(300);// on attend 300 ms
}// et on recommence la boucle

