"""
Ce programme calcule les différentes incertitudes-types :
    -- répétabilité
    -- résolution
    -- étalonnage
Puis il calcule l'incertitude-type composée
Et enfin l'incertitude élargie : 
    -- à l'aide de la fonction arrondi_sup qui arrondit à la décimale choisie supérieure
----------------------------------------------------------------------------------------
Les résultats de la compilation se trouvent en bas du programme
----------------------------------------------------------------------------------------
"""

# ----------------------------------
from math import sqrt,ceil
import numpy as np
import csv
# ----------------------------------
def arrondi_sup(n, decimals = 0):  
    multiplier = 10 ** decimals  
    return ceil(n * multiplier) / multiplier 
# ----------------------------------

# ----------------------------------
# placement des valeurs dans une liste
file = open("humidite0.csv", "r")
L=[]
lecteur=csv.reader(file)
for ligne in lecteur : 
    elmt=int(ligne[0])
    L.append(elmt)
file.close()
# ----------------------------------

# ----------------------------------
# calculs de moy et écart-type, nombre et affichage
moyenne = sum(L)/len(L)
s = np.std(L,ddof=1)# écart-type/ ddof pr dénom à n-1 (écart type corrigé)
nombre=len(L)
print("moyenne : "+str(moyenne))
print("écart-type : "+str(s))
print("nombre de mesures : "+str(nombre))
# ----------------------------------

print("--------------------------------------")
print("--------------------------------------")

# ----------------------------------
# La valeur mesurée
valeurMesuree=round(moyenne,1)
print("Valeur Mesurée : "+str(valeurMesuree))
# ----------------------------------

print("--------------------------------------")

# ----------------------------------
# Incertitude de répétabilité
u_repetabilite= round(s/nombre**(1/2),4)
print("u_rep : "+str(u_repetabilite))
# ----------------------------------

print("--------------------------------------")

# ----------------------------------
# Incertitude de résolution
resolution=1
u_resolution = round(resolution/(2*sqrt(3)), 4)
print("u_res : "+str(u_resolution))
# ----------------------------------

print("--------------------------------------")

# ----------------------------------
# Incertitude d'étalonnage (constructeur)
EMT = 2 # cf datasheet : erreur max tolérée
u_etalonnage = round(EMT/sqrt(3),3)
print("u_etal : "+str(u_etalonnage))
# ----------------------------------

print("--------------------------------------")
print("--------------------------------------")

# ----------------------------------
# incertitude-type composée
u = sqrt(u_repetabilite**2 + u_resolution**2 + u_etalonnage**2)
u = round(u,3)
print("u(N) : "+str(u))
# ----------------------------------

print("--------------------------------------")

# ----------------------------------
# facteur d'élargissement
k=2
U=k*u
U=arrondi_sup(U, 1)
print('U(N) : '+str(U))
# ----------------------------------

print("--------------------------------------")
print("--------------------------------------")

# ----------------------------------
# écriture du résultat
print("N_mesure = "+str(valeurMesuree)+" $\\pm$ "+str(U))
"""
moyenne : 3.294736842105263
écart-type : 3.9831404170930558
nombre de mesures : 380
--------------------------------------
--------------------------------------
Valeur Mesurée : 3.3
--------------------------------------
u_rep : 0.2043
--------------------------------------
u_res : 0.2887
--------------------------------------
u_etal : 1.155
--------------------------------------
--------------------------------------
u(N) : 1.208
--------------------------------------
U(N) : 2.5
--------------------------------------
--------------------------------------
N_mesure = 3.3 $\pm$ 2.5
"""
