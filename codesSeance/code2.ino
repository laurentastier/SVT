void setup() {
  Serial.begin(9600);
}

void loop() {
  int nbeMesures=50;
  long somme=0;// une variable pour sommer les 50 mesures
  for (int mesure=1;mesure<=nbeMesures;mesure=mesure+1){
    int humidite = analogRead(A0);
    somme=somme+humidite;// on incrémente la somme totale
    delay(100);//un délai d'attente entre chaque mesure
  }
  float moyenne = somme/50.0;//on calcule la moyenne des 50 mesures
  Serial.println(moyenne,2);// et on l'affiche avec 2 chiffres significatifs
  delay(2);// petit delay avant de reboucler
}
