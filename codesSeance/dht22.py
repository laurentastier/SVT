"""
Ce programme calcule les différentes incertitudes-type : 
    -- pour la température : 
        -- incertitude-type d'étalonnage
        -- incertitude-type de résolution
        -- incertitude-type de répétabilité
    -- pour l'humidité : 
        -- incertitude-type d'étalonnage
        -- incertitude-type de résolution
        -- incertitude-type de répétabilité
        -- incertitude-type d'hystérésis
        -- incertitude-type de stabilité
Puis il calcule les 2 incertitudes-types composées
Enfin il donne l'incertitude élargie grâce à la fonction arrondi_sup (qui arrondit à la décimale supérieure)
-----------------------------------------------------------------------------------------------------------
Les résultats de la compilation se trouvent en dessous du programme
-----------------------------------------------------------------------------------------------------------
"""

from math import sqrt,ceil
# ----------------------------------
def arrondi_sup(n, decimals = 0):
    multiplier = 10 ** decimals
    return ceil(n * multiplier) / multiplier
# ----------------------------------


print("étude pour la température : ")
# pour le capteur
#-------------------------------
etalonnage = 0.5
uT_etalonnage = round(etalonnage/sqrt(3),4)
print("uT_etalonnage = ", uT_etalonnage, "°C")
#-------------------------------
resolution = 0.1
uT_resolution = round(resolution/(2*sqrt(3)),5)
print("uT_resolution = ", uT_resolution)
#-------------------------------
repetatibilite = 0.2
uT_repetatibilite = round(repetatibilite/sqrt(3),4)
print("uT_repetatibilite = ", uT_repetatibilite, "°C")
#-------------------------------
uT_capteur = sqrt(uT_etalonnage**2 + uT_resolution**2 + uT_repetatibilite**2)
uT_capteur = round(uT_capteur, 4)
print("uT_capteur = ", uT_capteur, "°C")
#-------------------------------
k = 2 # facteur d'agrandissement
UT_capteur = k*uT_capteur
UT_capteur = arrondi_sup(UT_capteur, 1)
print("U(T)_capteur = ", UT_capteur, "°C")
#-------------------------------

print("-----------------------------------")

print("étude pour l'humidité : ")
# pour le capteur
#-------------------------------
etalonnage = 5
uH_etalonnage = round(etalonnage/sqrt(3),3)
print("uH_etalonnage = ", uH_etalonnage, " %")
#-------------------------------
resolution = 0.1
uH_resolution = round(resolution/(2*sqrt(3)),5)
print("uH_resolution = ", uH_resolution, " %")
#-------------------------------
repetatibilite = 1
uH_repetatibilite = round(repetatibilite/sqrt(3),4)
print("uH_repetatibilite = ", uH_repetatibilite, " %")
#-------------------------------
hysteresis = 0.3
uH_hysteresis = round(hysteresis/sqrt(3), 4)
print("uH_hysteresis = ", uH_hysteresis, " %")
#-------------------------------
stabilite = 0.5
uH_stabilite = round(stabilite/sqrt(3), 4)
print("uH_stabilite = ", uH_stabilite, " %")
#-------------------------------
uH_capteur = sqrt(uH_etalonnage**2 + uH_resolution**2 + uH_repetatibilite**2 + uH_hysteresis**2 + uH_stabilite**2)
uH_capteur = round(uH_capteur, 4)
print("uH_capteur = ", uH_capteur, " %")
#-------------------------------
k = 2 # facteur d'agrandissement
UH_capteur = k*uH_capteur
UH_capteur = arrondi_sup(UH_capteur, 1)
print("U(H)_capteur = ", UH_capteur, " %")

"""
étude pour la température : 
uT_etalonnage =  0.2887 °C
uT_resolution =  0.02887
uT_repetatibilite =  0.1155 °C
uT_capteur =  0.3123 °C
U(T)_capteur =  0.7 °C
-----------------------------------
étude pour l'humidité : 
uH_etalonnage =  2.887  %
uH_resolution =  0.02887  %
uH_repetatibilite =  0.5774  %
uH_hysteresis =  0.1732  %
uH_stabilite =  0.2887  %
uH_capteur =  2.9635  %
U(H)_capteur =  6.0  %
"""
