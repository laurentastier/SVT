void setup() {
  pinMode(7, OUTPUT);
}
void loop() {
     int humidite = analogRead(A0);// on prend la mesure d'humidité
     int TropSec = 49;//limite basse
     int TropHumide = 700;// limite haute
	 //------------------------------------------------------------
	 // ---- cas terre trop sèche----------------------------------
	 //------------------------------------------------------------
     if (humidite <= TropSec){// si humidité trop basse
       digitalWrite(7, HIGH);// on allume la lampe
     }
	 //------------------------------------------------------------
	 // ---- cas terre trop humide --------------------------------
	 //------------------------------------------------------------
     if (humidite >= TropHumide){// si humidité trop haute
       digitalWrite(7,HIGH);//on fait clignoter la LED
       delay(200);//...
       digitalWrite(7,LOW);//...
       delay(200);//...
     }
	 //------------------------------------------------------------
	 // ---- cas humidité ok --------------------------------------
	 //------------------------------------------------------------
     if ((humidite > TropSec) and (humidite < TropHumide)){//
     digitalWrite(7,LOW);// lampe éteinte
     }
	 //------------------------------------------------------------
  delay(2);//un petit delay avant de reboucler
}
