const int led=8;
void setup() {
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  digitalWrite(led, HIGH);
  Serial.println("LED allumée");
  delay(3000);
  digitalWrite(led, LOW);
  Serial.println("LED éteinte");
  delay(2000);
}
